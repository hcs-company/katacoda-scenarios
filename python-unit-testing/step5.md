As we saw when examining the code coverage, there is room for improvement in the `game.py` implementation. To improve this, we should rewrite the control loop and use the `inverse of control` technique. 

Refactoring of the control loop is probably too much effort for this LAB, but if you like to try it, go for it! However, if you just want to compare the results, you can go to the `solution` directly.

`cd ~/tictactoe/solution`{{ execute }}

If we run the code coverage again, we can see a big improvement:
`coverage run -m pytest --cov tictactoe --cov-report term-missing`{{ execute }}

The actual implementation can be seen in the code editor. In the solution `tictactoe` folder you can see two additional files: `player.py` and `computer.py`. These modules implement all control interaction and use the same interface. They both implement the method `get_move_coord`, which is used by the `game.py` control loop. Since all external interaction is now moved to the implementation of the module, the control loop itself can be tested as well (by providing a mocked for these player objects).

If you [like to play a game](https://www.imdb.com/title/tt0086567/), a quick last excercise could be to have the computer play by itself ;-)

This was the last step of the LAB. I hope you enjoyed it :-) If you want to revisit the slides for this lab, they can be found [here](https://slides.hcs-company.com/slides/python-unit-testing).