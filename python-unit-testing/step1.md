Welcome to the LAB setup for the HCS Python Unit Testing connect cafe session. In this scenario you can play around with a prepared python project "tic-tac-toe". In the editor (top-window) you can view and edit the code of this project. You will see an `exercise` and a `solution` folder. The idea is to play around with the code present in the `exercise` folder. The `solution` folder can be used as a reference, and contains an improved final version for the game.

# Code organisation

If you open up the `exercise` folder you will find two additional folders and a `main.py`. Open up the `main.py` file, and notice that it will import a module `tictactoe`. To be able to test the code, the code needs to be in modules, hence `main.py` immediately delegates to this module. If you look in the folder list, you will see both a `tictactoe` folder and a `tests` folder. For now, we will look at `tictactoe` only.

Open up the `tictactoe` folder in your editor, and you will see `game.py` and `board.py`. The `game.py` module is the actual game implementation, while the `board.py` implements a tic-tac-toe board with a bunch of handy methods. The `put` and `get` functions places/retrieves the pieces on the board at given coordinates. `can_move` checks if it's still possible to place something on the board. `has_winner` will check if there is a winner. The `get_winner` function will return the winner; where E(empty) means there is no winner at all.

# Play the game

Since you know a bit how the project is setup, you are probably thrilled to play this exciting game! 
Just to be sure, in the shell, go to the excercise folder: `cd ~/tictactoe/exercise`{{ execute }}
And run the game with: `python main.py`{{ execute }}