But, we are not here for playing tic-tac-toe! When we looked at the project, we also saw a folder called `tests`. This is the folder that contains all the implemented tests for the game. Open up the `tests` folder in the editor and you will see two files: `board_test.py` and `game_test.py`. As a best practice, the files have the same name as the implementation they will test, and have '_test' as a prefix. The latter will make it easy to 'discover' the available unittests.

First let's switch over to the folder:
`cd ~/tictactoe/exercise`{{ execute }}

And let's execute the available unittests in there:
`python -m unittest discover tests/ -p "*_test.py"`{{execute}}

You should now see that one test failed. In the next step we will fix that.